"""the module includes a few tools used in the other three modules"""
import shutil
import os


def file_name_from_path(file_path):
    """takes the path and returns the filename by splitting up the path"""
    path_first_split = file_path.split("/")
    path_second_split = path_first_split[-1].split("\\")
    path_final_split = path_second_split[-1].split(".")
    file_name = path_final_split[0]
    return file_name


def moves_files_to_different_directories(location_from_to_move, location_to_move_file):
    """moves any file from one place to another"""
    shutil.move(location_from_to_move, location_to_move_file)


def the_file_path_without_the_extension(file_path):
    """cuts the path so that it has no extension and returns the path without the extension"""
    splitted_path = file_path.split(".")
    return splitted_path[0]


def the_file_extension(file_path):
    """cuts the extension out of the path and returns the extension"""
    splitted_path = file_path.split(".")
    return splitted_path[1]


def search_engine_for_finding_files(file_name, search_path):
    """incredibly heavy search engine
     searches for a file (file_name parameter) in a path (search_path parameter)"""
    result = []
    for root, dirs, files in os.walk(search_path):
        if file_name in files:
            result.append(os.path.join(root, file_name))
    return result
