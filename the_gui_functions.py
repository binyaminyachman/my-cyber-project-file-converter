"""this file contains all the functions used to create the gui frame and anything else connected to the gui"""
import os
import tkinter as tk
from tkinter import ttk
from tkinter.messagebox import showinfo
from tkinter import filedialog as fd
import the_converters
import useful_side_functions
import convert_to_txt

WINDOW_SIZE = '400x400'
WINDOW_TITLE = 'converter'
FILENAME_BUTTON_TITLE = 'Open a File'
TXT_EXTENSION = ".txt"
DOCX_BUTTON_TEXT = "convert to docx"
PDF_BUTTON_TEXT = "convert to pdf"
HTML_BUTTON_TEXT = "convert to html"
TXT_BUTTON_TEXT = "keep the txt conversion and close"
TXT_AND_CLOSE_BUTTON_TEXT = "close and remove txt"


class Gui:
    def __init__(self, master):
        self.master = master
        self.frame = tk.Frame(self.master)
        self.open_file_button = ttk.Button(
            self.frame,
            text=FILENAME_BUTTON_TITLE,
            command=self.select_file()
        )
        convert_to_txt.convert_given_file_to_txt(self.filename)
        self.filepath_as_a_txt_file = (useful_side_functions.the_file_path_without_the_extension(self.filename) +
                                       TXT_EXTENSION)
        self.open_file_button.pack(expand=True)
        self.docx_button = ttk.Button(self.frame, text=DOCX_BUTTON_TEXT,
                                      command=lambda: self.the_command_for_the_docx_converter_button(
                                          self.filepath_as_a_txt_file))
        self.docx_button.pack(fill=tk.X, padx=5, pady=5)
        self.pdf_button = ttk.Button(self.frame, text=PDF_BUTTON_TEXT,
                                     command=lambda: self.the_command_for_the_pdf_converter_button(
                                         self.filepath_as_a_txt_file))
        self.pdf_button.pack(fill=tk.X, padx=5, pady=5)
        self.html_button = ttk.Button(self.frame, text=HTML_BUTTON_TEXT,
                                      command=lambda: self.the_command_for_the_html_converter_button(
                                          self.filepath_as_a_txt_file))
        self.html_button.pack(fill=tk.X, padx=5, pady=5)
        self.save_txt_and_close_button = ttk.Button(self.frame, text=TXT_BUTTON_TEXT,
                                                    command=quit)
        self.save_txt_and_close_button.pack(fill=tk.X, padx=5, pady=5)
        self.remove_txt_and_close_button = ttk.Button(self.frame, text=TXT_AND_CLOSE_BUTTON_TEXT,
                                                      command=lambda: self.command_for_closing_and_removing_the_txt_file(
                                                   self.filepath_as_a_txt_file))
        self.remove_txt_and_close_button.pack(fill=tk.X, padx=5, pady=5)
        self.frame.pack()

    def select_file(self):
        """the first part of the GUI's action.
         opens the File Explorer and lets you choose a file from the files on your computer.
         the file is run using the button in the button_open function"""
        filetypes = (
            ('text files', '*.txt'),
            ('html files', '*.html'),
            ('pdf files', '*.pdf'),
            ('docx files', '*.docx')
        )
        self.filename = fd.askopenfilename(
            title=FILENAME_BUTTON_TITLE,
            initialdir='\\',
            filetypes=filetypes)

    def the_command_for_the_docx_converter_button(self, txt_file_path):
        """connecting the docx button to the converting function"""
        the_converters.txt_to_docx_converting_code(txt_file_path)
        showinfo(title="converted!",
                 message="the file has converted to docx successfully")

    def the_command_for_the_pdf_converter_button(self, txt_file_path):
        """connects the pdf button to the converting function"""
        the_converters.converts_txt_file_to_pdf_file(txt_file_path)
        showinfo(title="converted!",
                 message="the file has converted to pdf successfully")

    def the_command_for_the_html_converter_button(self, txt_file_path):
        """connects the html button to the converting function"""
        the_converters.converts_txt_files_to_html_files(txt_file_path)
        showinfo(title="converted!",
                 message="the file has converted to html successfully")

    def command_for_closing_and_removing_the_txt_file(self, txt_file_path):
        """the command connected to close() function"""
        os.remove(useful_side_functions.the_file_path_without_the_extension(txt_file_path) + ".txt")
        self.master.quit()
