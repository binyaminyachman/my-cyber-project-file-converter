"""this file contains the main function and runs it. the other three files are
imported and the run order of the files decided"""
import tkinter as tk
import the_gui_functions


def main():
    """the main function. first function to run. runs all the other
    functions contained in the other files"""
    root = tk.Tk()
    the_gui_instance = the_gui_functions.Gui(root)
    root.mainloop()


if __name__ == '__main__':
    main()
