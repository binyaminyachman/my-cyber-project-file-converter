"""this module contains all the functions which involve converting the different file types"""
import os
import codecs
import sys
import pdfplumber
from fpdf import FPDF
from docx import Document
from bs4 import BeautifulSoup
import docx2txt
import useful_side_functions


HTML_TAGS_USED_IN_EVERY_SENTENCE = "<body><p>{sentence}\n</p></body>"
HTML_TAGS_USED_IN_EVERY_TITLE = "<title>{path}</title>"
HTML_BEGINNING_TAG = "<!DOCTYPE html>"
HTML_UTF_8_ENCODING_TAG = '<meta charset="utf-8"/>'
UTF_8_ENCODING = "utf-8"
LATIN_1_ENCODING = "latin-1"
DOCX_EXTENSION = ".docx"
TXT_EXTENSION = ".txt"
PDF_EXTENSION = ".pdf"
HTML_EXTENSION = ".html"
ONE_ENTER = "\n"
TWO_ENTERS = "\n\n"
TAB = "\t"
SPACE = " "
OPEN_FOR_READING = "r"
OPEN_FOR_WRITING = "w"
ARIAL_FONT = "Arial"
PDF_TEXT_SIZE = 10
MAX_SENTENCE_LENGTH = 100
LOCAL_PATH = "F"
CENTRAL_ALIGN = "C"
C_DIRECTORY_PATH = "c:/"
EMPTY_FILE_MESSAGE = "file is empty"


def txt_to_docx_converting_code(txt_file_path):
    """converts txt files into docx files, by creating/editing
     a docx file and writing in the txt file's text"""
    document = Document()
    try:
        with open(txt_file_path, OPEN_FOR_READING) as text_file:
            full_text = text_file.read()

    except UnicodeDecodeError:
        with open(txt_file_path, OPEN_FOR_READING, encoding=UTF_8_ENCODING) as text_file:
            full_text = text_file.read()
    paragraphs_of_full_text = full_text.split(ONE_ENTER)
    for paragraph in paragraphs_of_full_text:
        document.add_paragraph(paragraph)
    document.save(
        useful_side_functions.the_file_path_without_the_extension(txt_file_path) +
        DOCX_EXTENSION)


def converts_docx_files_to_txt_files(docx_file_path):
    """converts docx files into txt files."""
    target_path = useful_side_functions.the_file_path_without_the_extension(docx_file_path) + TXT_EXTENSION
    the_text_in_the_docx_file = docx2txt.process(docx_file_path).replace(TWO_ENTERS, ONE_ENTER)
    with open(target_path, OPEN_FOR_WRITING, encoding=UTF_8_ENCODING) as target_text_file:
        target_text_file.write(the_text_in_the_docx_file)


def converts_txt_file_to_pdf_file(txt_file_path):
    """converts txt files into pdf files"""
    file_name = useful_side_functions.file_name_from_path(txt_file_path)
    dst_folder_path = useful_side_functions.the_file_path_without_the_extension(txt_file_path) + PDF_EXTENSION
    pdf = FPDF()
    pdf.add_page()
    pdf.set_font(ARIAL_FONT, size=PDF_TEXT_SIZE)
    try:
        the_pdf_file = useful_side_functions.the_file_path_without_the_extension(txt_file_path) + PDF_EXTENSION
        os.remove(the_pdf_file)
        print(the_pdf_file + " Removed!")
    except:
        pass
    with open(txt_file_path, OPEN_FOR_READING, encoding=UTF_8_ENCODING) as file:
        full_text_list = file.read().split(ONE_ENTER)
        for sentence in full_text_list:
            if len(sentence) > MAX_SENTENCE_LENGTH:
                for a_hundred_words in range(0, int(len(sentence)/100)+1):
                    encoded_text1 = sentence[a_hundred_words * 100:(a_hundred_words+1)*100].encode(
                        UTF_8_ENCODING).decode(LATIN_1_ENCODING)
                    pdf.cell(200, 10, txt=encoded_text1, ln=10, align=CENTRAL_ALIGN)
            else:
                encoded_text2 = sentence.encode(UTF_8_ENCODING).decode(LATIN_1_ENCODING)
                pdf.cell(200, 10, txt=encoded_text2, ln=10, align=CENTRAL_ALIGN)
        pdf.output(file_name + PDF_EXTENSION)

    the_created_file_directory_list = useful_side_functions.search_engine_for_finding_files(file_name +
                                                                                            PDF_EXTENSION,
                                                                                            C_DIRECTORY_PATH)
    the_created_file_directory = the_created_file_directory_list[0]
    useful_side_functions.moves_files_to_different_directories(the_created_file_directory, dst_folder_path)


def converts_pdf_files_to_txt_files(path):
    """converts pdf files to txt files"""
    with pdfplumber.open(path) as pdf:
        with open(useful_side_functions.the_file_path_without_the_extension(path)
                  + TXT_EXTENSION, OPEN_FOR_WRITING, encoding=UTF_8_ENCODING) as output_txt_file:
            for page in range(len(pdf.pages)):
                my_page = pdf.pages[page]
                warped_text_page = my_page.extract_text()
                text_page = str(warped_text_page).replace(TAB, SPACE)
                try:
                    output_txt_file.write(text_page)
                except:
                    print(EMPTY_FILE_MESSAGE)
                    sys.exit()


def converts_html_files_to_txt_files(path):
    """converts html files to txt files"""
    with codecs.open(path, OPEN_FOR_READING, UTF_8_ENCODING) as html_file:
        with open(useful_side_functions.the_file_path_without_the_extension(path) + TXT_EXTENSION,
                  OPEN_FOR_WRITING, encoding=UTF_8_ENCODING) as text_file:
            html_file_full_text_content = BeautifulSoup(html_file)
            html_file_full_text_content_in_text = html_file_full_text_content.get_text()
            text_file.write(html_file_full_text_content_in_text)


def converts_txt_files_to_html_files(txt_file_path):
    """converts txt files to html files"""
    try:
        with open(txt_file_path, OPEN_FOR_READING, encoding=UTF_8_ENCODING) as text_file:
            with codecs.open(useful_side_functions.the_file_path_without_the_extension(txt_file_path) + HTML_EXTENSION,
                             OPEN_FOR_WRITING, UTF_8_ENCODING) as html_file:
                full_text_list = text_file.read().split(ONE_ENTER)
                html_file.write(HTML_BEGINNING_TAG)
                html_file.write(HTML_TAGS_USED_IN_EVERY_TITLE.format(path=useful_side_functions.file_name_from_path(
                    txt_file_path)))
                html_file.write(HTML_UTF_8_ENCODING_TAG)
                for sentence in full_text_list:
                    html_file.write(HTML_TAGS_USED_IN_EVERY_SENTENCE.format(sentence=sentence))
    except UnicodeDecodeError:
        with open(txt_file_path, OPEN_FOR_READING) as text_file:
            with codecs.open(useful_side_functions.the_file_path_without_the_extension(txt_file_path) + HTML_EXTENSION,
                             OPEN_FOR_WRITING, UTF_8_ENCODING) as html_file:
                full_text_list = text_file.read().split("\n")
                html_file.write(HTML_BEGINNING_TAG)
                html_file.write(HTML_TAGS_USED_IN_EVERY_TITLE.format(path=useful_side_functions.file_name_from_path(
                    txt_file_path)))
                html_file.write(HTML_UTF_8_ENCODING_TAG)
                for sentence in full_text_list:
                    html_file.write(HTML_TAGS_USED_IN_EVERY_SENTENCE.format(sentence=sentence))
