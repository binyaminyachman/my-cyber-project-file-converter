import the_converters
import useful_side_functions

DOCX_EXTENSION_TYPE = "docx"
PDF_EXTENSION_TYPE = "pdf"
HTML_EXTENSION_TYPE = "html"


def convert_given_file_to_txt(chosen_file_path):
    chosen_extension = useful_side_functions.the_file_extension(chosen_file_path)
    if chosen_extension == DOCX_EXTENSION_TYPE:
        the_converters.converts_docx_files_to_txt_files(chosen_file_path)
    elif chosen_extension == PDF_EXTENSION_TYPE:
        the_converters.converts_pdf_files_to_txt_files(chosen_file_path)
    elif chosen_extension == HTML_EXTENSION_TYPE:
        the_converters.converts_html_files_to_txt_files(chosen_file_path)
